﻿using System;

namespace Uso_De_Archivos
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombreArchivo = "";
            Program program = new Program();


            string leerArchivo()
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombreArchivo = Console.ReadLine();
                nombreArchivo = nombreArchivo + ".txt";
                using (FileStream fs = new FileStream(nombreArchivo, FileMode.OpenOrCreate))
                {
                    fs.Close();
                }
                return nombreArchivo;
            }

            void CrearArchivo(string nombreArchivo)
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombre = Console.ReadLine();
                nombre += ".txt";
                if (nombre.Equals(nombreArchivo))
                {
                    using (StreamWriter sw = File.AppendText(nombre))
                    {
                        Console.Write("Ingrese el texto: ");
                        string texto = Console.ReadLine();
                        sw.WriteLine(texto);
                    }
                }
                else
                    Console.WriteLine("Error, nombre incorrecto");
            }

            void leerArchivo(string nombreArchivo)
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombre = Console.ReadLine();
                nombre += ".txt";
                if (nombre.Equals(nombreArchivo))
                {
                    using (StreamReader sr = File.OpenText(nombre))
                    {
                        if (new FileInfo(nombre).Length != 0)
                        {
                            string texto = "";
                            while ((texto = sr.ReadLine()) != null)
                            {
                                Console.WriteLine(texto);
                            }
                        }
                        else
                            Console.WriteLine("El archivo no tiene ningún texto");
                    }
                }
                else
                    Console.WriteLine("Error, nombre incorrecto");
                Console.WriteLine("Presione cualquier tecla para continuar");
                Console.ReadKey();
            }

            void borrarContenido(string nombreArchivo)
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombre = Console.ReadLine();
                nombre += ".txt";
                if (nombre.Equals(nombreArchivo))
                {
                    using FileStream fs = new FileStream(nombre, FileMode.Truncate);
                    fs.Close();
                }
                else
                    Console.WriteLine("Error, nombre incorrecto");
            }

        }
    }
}



