#include <iostream>
#include <fstream>
#include <string.h>
void crearArchivo();
void leerArchivo();
void borrarArchivo();
bool existeArchivo(char nombreArchivo[100]);
int main() {
    borrarArchivo();
    system("pause");
    return 0;
}
void crearArchivo() {
    char nombreArchivo[100], textoArchivo[100];
    std::cout << "Ingresa el nombre de tu archivo: ";
    std::cin.getline(nombreArchivo, 100, '\n');
    std::cout << "Ingresa el texto de tu archivo:\n";
    std::cin.getline(textoArchivo, 100, '\n');
    std::fstream archivo;
    archivo.open(nombreArchivo, std::ios::out);
    if (archivo.fail() == true) {
        std::cout << "No se pudo crear\n";
        return;
    }
    archivo << textoArchivo;
}
void leerArchivo() {
    std::string texto;
    std::fstream archivo;
    char nombreArchivo[50];
    std::cout << "Que archivo quieres ver:";
    std::cin >> nombreArchivo;
    archivo.open(nombreArchivo, std::ios::in);
    if (archivo.fail() == true) {
        std::cout << "No se pudo leer tu archivo por: " << strerror(errno) << "\n";
        return;
    }
    while (!archivo.eof()) {
        std::getline(archivo, texto);
        std::cout << texto << "\n";
    }

}
void borrarArchivo() {
    char nombreArchivo[100];
    std::cout << "Ingresa el nombre del archivo a borrar:";
    std::cin >> nombreArchivo;
    if (existeArchivo(nombreArchivo) == true) {
        if (remove(nombreArchivo) == 0) {
            std::cout << "El archivo " << nombreArchivo << " fue borrado existosamente\n";
        }
    }
    else {
        std::cout << "El archivo " << nombreArchivo << " no existe\n";
    }
}
bool existeArchivo(char nombreArchivo[100]) {
    std::fstream archivo;
    archivo.open(nombreArchivo, std::ios::in);
    if (archivo.good() == true) {
        return true;
    }
    else {
        return false;
    }
}