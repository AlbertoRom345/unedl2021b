﻿
namespace ParcialSegundo
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCargarArchivo = new System.Windows.Forms.Label();
            this.lblLeerArchivo = new System.Windows.Forms.Label();
            this.lblProcesarArchivo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocalidadInventario = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.txtPuerto = new System.Windows.Forms.TextBox();
            this.textLocalidadBase = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.txtSalida = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblCargarArchivo
            // 
            this.lblCargarArchivo.AutoSize = true;
            this.lblCargarArchivo.Location = new System.Drawing.Point(139, 32);
            this.lblCargarArchivo.Name = "lblCargarArchivo";
            this.lblCargarArchivo.Size = new System.Drawing.Size(136, 15);
            this.lblCargarArchivo.TabIndex = 4;
            this.lblCargarArchivo.Text = "Localidad del Inventario ";
            // 
            // lblLeerArchivo
            // 
            this.lblLeerArchivo.AutoSize = true;
            this.lblLeerArchivo.Location = new System.Drawing.Point(139, 70);
            this.lblLeerArchivo.Name = "lblLeerArchivo";
            this.lblLeerArchivo.Size = new System.Drawing.Size(133, 15);
            this.lblLeerArchivo.TabIndex = 5;
            this.lblLeerArchivo.Text = "Opciones de Instalacion";
            // 
            // lblProcesarArchivo
            // 
            this.lblProcesarArchivo.AutoSize = true;
            this.lblProcesarArchivo.Location = new System.Drawing.Point(139, 102);
            this.lblProcesarArchivo.Name = "lblProcesarArchivo";
            this.lblProcesarArchivo.Size = new System.Drawing.Size(85, 15);
            this.lblProcesarArchivo.TabIndex = 6;
            this.lblProcesarArchivo.Text = "Localidad Base";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(139, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Direccion Ip";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Puerto de Comunicaccion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(139, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Password de Instalacion";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 256);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Sistema de Redundancia";
            // 
            // txtLocalidadInventario
            // 
            this.txtLocalidadInventario.Location = new System.Drawing.Point(310, 24);
            this.txtLocalidadInventario.Name = "txtLocalidadInventario";
            this.txtLocalidadInventario.Size = new System.Drawing.Size(201, 23);
            this.txtLocalidadInventario.TabIndex = 11;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(310, 213);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(201, 23);
            this.txtPassword.TabIndex = 12;
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(310, 179);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(201, 23);
            this.txtIp.TabIndex = 13;
            // 
            // txtPuerto
            // 
            this.txtPuerto.Location = new System.Drawing.Point(310, 141);
            this.txtPuerto.Name = "txtPuerto";
            this.txtPuerto.Size = new System.Drawing.Size(201, 23);
            this.txtPuerto.TabIndex = 14;
            // 
            // textLocalidadBase
            // 
            this.textLocalidadBase.Location = new System.Drawing.Point(310, 102);
            this.textLocalidadBase.Name = "textLocalidadBase";
            this.textLocalidadBase.Size = new System.Drawing.Size(201, 23);
            this.textLocalidadBase.TabIndex = 15;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(238, 301);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 18;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(310, 62);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(201, 23);
            this.comboBox1.TabIndex = 19;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(310, 248);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(201, 23);
            this.comboBox2.TabIndex = 20;
            // 
            // txtSalida
            // 
            this.txtSalida.Location = new System.Drawing.Point(68, 330);
            this.txtSalida.Name = "txtSalida";
            this.txtSalida.Size = new System.Drawing.Size(443, 23);
            this.txtSalida.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtSalida);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.textLocalidadBase);
            this.Controls.Add(this.txtPuerto);
            this.Controls.Add(this.txtIp);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtLocalidadInventario);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblProcesarArchivo);
            this.Controls.Add(this.lblLeerArchivo);
            this.Controls.Add(this.lblCargarArchivo);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Lector Archivos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblCargarArchivo;
        private System.Windows.Forms.Label lblLeerArchivo;
        private System.Windows.Forms.Label lblProcesarArchivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocalidadInventario;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.TextBox txtPuerto;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textLocalidadBase;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox txtSalida;
    }
}

