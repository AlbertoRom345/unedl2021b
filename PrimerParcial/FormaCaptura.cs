﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{

    public partial class FormaCaptura : Form
    {
        private static DataTable Array2dToDatatable(String[,] arreglo)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Nombre");
            dt.Columns.Add("Apellido");
            for (int i = 0; i < arreglo.GetLength(0); i++)
            {
                DataRow row = dt.NewRow();
                for (int j = 0; j < arreglo.GetLength(1); j++)
                {
                    row[j] = arreglo[i, j];
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        private static void ShowTable(DataTable dt)
        {
            foreach (DataColumn col in dt.Columns)
            {
                Console.WriteLine(col.ColumnName);
            }
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    Console.WriteLine(row[col]);
                }
            }
        }


        string[,] personas = new string[,]
        { { "Angel","Duran" },
          { "Christopher","Villalobos" },
          { "Daniel","Lopez" },
          { "Daniel","Vazquez" },
          { "Edgar","Banuelos" },
          { "Fernando","Hernandez" },
          { "Francisco","Garcia" },
          { "Ivan","Narvaez" },
          { "Joel","Juarez" },
          { "Juan","Romero" },
          { "Kevin","Gonzalez" },
          { "Luis","Gomez" },
          { "Manuel","Mariscal" },
          { "Mario","Mercado" },
          { "Marisol","Benitez" },
          { "Mauricio","Castaneda" },
          { "Oscar","Ochoa" },
          { "Yahayra","Rodriguez" },
          { "Cesar", "De la Cruz" }
        };

        private String BuscaPersonas(String n, String a, String[,] p)
        {
            String text = "";
            int rows = p.GetLength(0);
            int cols = p.GetLength(1);
            string[,] o = new string[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                if (n.Equals(p))
                {
                    string nombre = p[i, 0];
                    for (int j=0;j< cols; j++)

                    {
                        if (a.Equals(o[i,j))
                        {
                            string apellido = p[i, j];
                            lblNombre.Text = "Si esta en la lista:";
                            txtBuscaApellido.Text = "";
                            txtBuscaNombre.Text = nombre + " " + apellido;
                        }
                        else
                        {
                            lblNombre.Text = " No se encuentra ese Nombre";
                        }
                    }
                }
            }
           
            return text;
        }
    

        public FormaCaptura()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            txtBuscaNombre.Text = txtBuscaNombre.Text.Trim();
            txtBuscaApellido.Text = txtBuscaApellido.Text.Trim();
            if (txtBuscaNombre.Text.Length != 0 && 
                txtBuscaApellido.Text.Length !=0)
            {
                BuscaPersonas(txtBuscaNombre.Text, txtBuscaApellido.Text,personas); 
            }

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtBuscaNombre.Text = "Nombre";
            txtBuscaApellido.Text = "Apellido";
            txtBuscaNombre.Text = "";
            txtBuscaApellido.Text = "";

        }
        
    }
}
