#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>

using namespace std;

char filename[] = "reportcard.csv";
char bckpfile[] = "reportcardnew.csv";

int find_record(int rollnum)
{
	vector<string> row;
	int roll, roll2, result;
	string line, word;

	// return not found by default
	result = 0;

	fstream fin;
	fin.open(filename, ios::in);
	getline(fin, line);
	while (!line.empty())
	{
		row.clear();

		// used for breaking words
		stringstream s(line);

		// read every column data of a row and
		// store it in a string variable, 'word'
		while (getline(s, word, ','))
		{
			// add all the column data
			// of a row to a vector
			row.push_back(word);
		}

		// convert string to integer for comparision
		roll2 = stoi(row[0]);

		// Compare the roll number
		if (roll2 == rollnum)
		{
			result = 1;
			break;
		}
		getline(fin, line);
	}
	fin.close();
	return result;
}

//
// Create a record
//
void create_record()
{
	// file pointer
	fstream fout;

	// opens an existing csv file or creates a new file.
	fout.open(filename, ios::out | ios::app);

	cout << "Enter the details of the student:" << endl;
	cout << "# roll name maths phy chem bio #" << endl;

	int i, roll, phy, chem, math, bio;
	string name;

	cin >> roll
		>> name
		>> math
		>> phy
		>> chem
		>> bio;

	if (find_record(roll) != 1)
	{
		// Insert the data to file
		fout << roll << ","
			<< name << ","
			<< math << ","
			<< phy << ","
			<< chem << ","
			<< bio << endl;
	}
	else
	{
		cout << "<ERROR> Record found\n";
		system("pause");
	}
	fout.close();
}

//

//	Read a Record
//
void read_record()
{
	// File pointer
	fstream fin;

	// Open an existing file
	fin.open(filename, ios::in);

	// Get the roll number
	// of which the data is required
	int rollnum, roll2, count = 0;

	cout << "Enter the roll number "
		<< "of the student to display details: ";

	cin >> rollnum;

	// Read the Data from the file
	// as String Vector
	vector<string> row;

	string line, word, temp;

	getline(fin, line);

	while (!line.empty())
	{
		row.clear();

		// used for breaking words
		stringstream s(line);

		// read every column data of a row and
		// store it in a string variable, 'word'
		while (getline(s, word, ','))
		{
			// add all the column data
			// of a row to a vector
			row.push_back(word);
		}

		// convert string to integer for comparision
		roll2 = stoi(row[0]);

		// Compare the roll number
		if (roll2 == rollnum) {

			// Print the found data
			count = 1;
			cout << "Details of Roll " << row[0] << " : \n";
			cout << "Name: " << row[1] << "\n";
			cout << "Maths: " << row[2] << "\n";
			cout << "Physics: " << row[3] << "\n";
			cout << "Chemistry: " << row[4] << "\n";
			cout << "Biology: " << row[5] << "\n";
			system("pause");
			fin.close();
			break;
		}
		// read an entire row and
		// store it in a string variable 'line'
		getline(fin, line);
	}
	if (count == 0)
	{
		cout << "Record not found\n";
		system("pause");
	}
	fin.close();
}

//
// Update a Record
//
void update_record()
{
	// File pointer
	fstream fin, fout;

	// Open an existing record
	fin.open(filename, ios::in);

	// Create a new file to store updated data
	fout.open(bckpfile, ios::out);

	int rollnum, roll1, marks, count = 0, i;
	char sub;
	int index, new_marks;
	string line, word;
	vector<string> row;

	// Get the roll number from the user
	cout << "Enter the roll number "
		<< "of the record to be updated: ";
	cin >> rollnum;

	// Get the data to be updated
	cout << "Enter the subject "
		<< "to be updated(M/P/C/B): ";
	cin >> sub;

	// Determine the index of the subject
	// where Maths has index 2,
	// Physics has index 3, and so on
	if (sub == 'm' || sub == 'M')
		index = 2;
	else if (sub == 'p' || sub == 'P')
		index = 3;
	else if (sub == 'c' || sub == 'C')
		index = 4;
	else if (sub == 'b' || sub == 'B')
		index = 5;
	else {
		cout << "Wrong choice.Enter again\n";
		update_record();
	}

	// Get the new marks
	cout << "Enter new marks: ";
	cin >> new_marks;

	// Traverse the file
	while (!fin.eof()) {
		row.clear();

		getline(fin, line);
		stringstream s(line);

		while (getline(s, word, ',')) {
			row.push_back(word);
		}

		roll1 = stoi(row[0]);
		int row_size = row.size();

		if (roll1 == rollnum) {
			count = 1;
			stringstream convert;

			// sending a number as a stream into output string
			convert << new_marks;

			// the str() converts number into string
			row[index] = convert.str();

			if (!fin.eof()) {
				for (i = 0; i < row_size - 1; i++) {

					// write the updated data
					// into a new file 'reportcardnew.csv'
					// using fout
					fout << row[i] << ", ";
				}

				fout << row[row_size - 1] << "\n";
			}
		}
		else {
			if (!fin.eof()) {
				for (i = 0; i < row_size - 1; i++) {

					// writing other existing records
					// into the new file using fout.
					fout << row[i] << ", ";
				}

				// the last column data ends with a '\n'
				fout << row[row_size - 1] << "\n";
			}
		}
		if (fin.eof())
			break;
	}
	if (count == 0)
		cout << "Record not found\n";

	fin.close();
	fout.close();

	// removing the existing file
	remove(filename);

	// renaming the updated file with the existing file name
	rename(bckpfile, filename);
}

//
// Delete Record
//
void delete_record()
{
	// Open FIle pointers
	fstream fin, fout;

	// Open the existing file
	fin.open(filename, ios::in);

	// Create a new file to store the non-deleted data
	fout.open(bckpfile, ios::out);

	int rollnum, roll1, marks, count = 0, i;
	char sub;
	int index, new_marks;
	string line, word;
	vector<string> row;

	// Get the roll number
	// to decide the data to be deleted
	cout << "Enter the roll number "
		<< "of the record to be deleted: ";
	cin >> rollnum;

	// Check if this record exists
	// If exists, leave it and
	// add all other data to the new file
	while (!fin.eof())
	{
		row.clear();
		getline(fin, line);
		stringstream s(line);

		while (getline(s, word, ','))
		{
			row.push_back(word);
		}

		int row_size = row.size();
		roll1 = stoi(row[0]);

		// writing all records,
		// except the record to be deleted,
		// into the new file 'reportcardnew.csv'
		// using fout pointer
		if (roll1 != rollnum)
		{
			if (!fin.eof())
			{
				for (i = 0; i < row_size - 1; i++)
				{
					fout << row[i] << ", ";
				}
				fout << row[row_size - 1] << "\n";
			}
		}
		else
		{
			count = 1;
		}
		if (fin.eof())
			break;
	}
	if (count == 1)
		cout << "Record deleted\n";
	else
		cout << "Record not found\n";
	system("pause");
	// Close the pointers
	fin.close();
	fout.close();

	// removing the existing file
	remove(filename);

	// renaming the new file with the existing file name
	rename(bckpfile, filename);
}

//
// Display menu
//
void display_menu()
{
	system("CLS");
	cout << "###### Main Menu ######" << endl;
	cout << "C) Create record" << endl;
	cout << "R) Read record" << endl;
	cout << "U) Update record" << endl;
	cout << "D) Delete record" << endl;
	cout << "O) Exit" << endl;
}

int main()
{
	char option;
	setlocale(LC_ALL, "");
	do
	{
		display_menu();
		cin >> option;
		switch (option) {
		case 'C':
			cout << "Create a record" << endl;
			create_record();
			break;
		case 'R':
			cout << "Read a record" << endl;
			read_record();
			break;
		case 'U':
			cout << "Update a record" << endl;
			update_record();
			break;
		case 'D':
			cout << "Delete a record" << endl;
			delete_record();
			break;
		case 'O':
			cout << "Exit" << endl;
			break;
		default:
			cout << "Invalid option" << endl;
			system("pause");
			break;
		}
	} while (option != 'O');
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
